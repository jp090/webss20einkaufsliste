from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from . import models


# Create your views here.


def index(request):
    return HttpResponse('Shopping App')


"""
def index(request):
    context = {}
    context['einkaufen'] = models.Item.objects.filter(erledigt=False)
    context['erledigt'] = models.Item.objects.filter(erledigt=True)
    return render(request, 'shopping/index.html', context)

def add(request):
    if 'add' in request.POST:
        item = models.Item()
        item.text = request.POST['text']
        item.menge = request.POST['menge']
        item.save()
    return redirect('index')

def delete(request):
    if 'id' in request.GET:
        item = get_object_or_404(models.Item, id=request.GET.get('id'))
        item.delete()
    return redirect('index') """